import cv2
import sys
import time
import numpy

import MMCorePy as MMCore
import py_spinSoftJS_AF as autoFocus

from PIL import Image
from PIL.ImageQt import ImageQt
from multiprocessing import Process

from PyQt5.QtCore import QThread, pyqtSignal
from PyQt5.QtGui import QIcon, QPixmap, QPen, QFont, QColor
from PyQt5.QtWidgets import QWidget, QPushButton, QGridLayout, QApplication,\
    QSpinBox, QLabel, QDoubleSpinBox, QGraphicsScene, QCheckBox,\
    QMessageBox

from SnapThread import SnapThread
from ZoomGraphicsView import ZoomGraphicsView
from os.path import join


# ==============================================================================
# BalanceRatioCheck
# ==============================================================================
class BalanceRatioCheck(QThread):
    balanceRatioText = pyqtSignal(str)

    def __init__(self, mmcore):
        QThread.__init__(self)
        self._mmcore = mmcore
        self._cameraName = ''

    def run(self, *args, **kwargs):
        while True:
            propName = "Camera/GenICam/AnalogControl/BalanceRatioSelector"
            self._mmcore.setProperty(self._cameraName, propName, "Red")
            propName = "Camera/GenICam/AnalogControl/BalanceRatio"
            redBalanceRatio = self._mmcore.getProperty(self._cameraName,
                                                       propName)

            propName = "Camera/GenICam/AnalogControl/BalanceRatioSelector"
            self._mmcore.setProperty(self._cameraName, propName, "Blue")
            propName = "Camera/GenICam/AnalogControl/BalanceRatio"
            blueBalanceRatio = self._mmcore.getProperty(self._cameraName,
                                                        propName)

            self.balanceRatioText.emit(str(redBalanceRatio) + "/" +
                                       str(blueBalanceRatio))
            time.sleep(0.5)


class LiveThread(QThread):
    snappedImage = pyqtSignal(QPixmap, list, list)

    def __init__(self, mmcore):
        QThread.__init__(self)
        self._mmcore = mmcore
        self._saturation = False
        self._metric = False
        self._autoFocus = autoFocus.AutoFocusInterface()

    def _getFocusMetric(self, band_img):
        band_img = numpy.uint8(band_img)
        focusVal = numpy.mean(abs(cv2.Sobel(band_img, cv2.CV_8U, 1, 0,
                                            ksize=3)))
        return focusVal

    def _getFocalVal(self, band_img, index, ifWrite):
        focusValVec = []
        intValVec = []
        for y in range(4):
            for x in range(4):
                smallBlock = band_img[y*304:y*304+304, x*484:x*484+484]
                (imgH, imgW) = smallBlock.shape
                focusVal = self._autoFocus.AutoFocusDriver(
                                smallBlock.astype(numpy.uint8),
                                imgW, imgH, index, ifWrite)
                # focusVal = self._getFocusMetric(smallBlock)

                intVal = numpy.mean(smallBlock.astype(numpy.uint8))
                focusValVec.append(focusVal)
                intValVec.append(intVal)
        (imgH, imgW) = band_img.shape
        focusVal = self._autoFocus.\
            AutoFocusDriver(band_img.astype(numpy.uint8),
                            imgW, imgH, index, ifWrite)
        # focusVal = self._getFocusMetric(band_img)
        focusValVec.append(focusVal)
        return focusValVec, intValVec

    def run(self, *args, **kwargs):
        while True:
            self._mmcore.snapImage()
            img = self._mmcore.getImage()

            imgWidth = self._mmcore.getImageWidth()
            imgHeight = self._mmcore.getImageHeight()

            img2 = img.view(dtype=numpy.uint8)
            imageArr = numpy.zeros((imgHeight, imgWidth, 3),
                                   dtype=numpy.uint8)

            row_sampling = 1
            col_sampling = 4

            # Flipped BGR to RGB because display need in RGB format.
            imageArr[:, :, 2] = img2[::row_sampling, ::col_sampling]
            imageArr[:, :, 1] = img2[::row_sampling, 1::col_sampling]
            imageArr[:, :, 0] = img2[::row_sampling, 2::col_sampling]
            if self._metric is True:
                fValVec, intvalVec = self._getFocalVal(imageArr[:, :, 1], 0, 0)
            else:
                fValVec = []
                intvalVec = []
            if self._saturation is True:
                max_saturation_val = 240

                # Check and find the channel having maximum saturation.
                Channel = imageArr[:, :, 0] > max_saturation_val
                imageArr[Channel] = [255, 0, 0]
                Channel = imageArr[:, :, 1] > max_saturation_val
                imageArr[Channel] = [255, 0, 0]
                Channel = imageArr[:, :, 2] > max_saturation_val
                imageArr[Channel] = [255, 0, 0]

            qImage = ImageQt(Image.fromarray(imageArr))
            self.snappedImage.emit(QPixmap.fromImage(qImage), fValVec,
                                   intvalVec)
            time.sleep(0.2)


# ==============================================================================
# WriteBufferToDisk
# ==============================================================================
class WriteBufferToDisk():
    '''
    Class to write a column of images to the disk. Method for writing will
    be called via a process.
    '''
# |----------------------------------------------------------------------------|
# class Variables
# |----------------------------------------------------------------------------|
#    no classVariables

# |----------------------------------------------------------------------------|
# start
# |----------------------------------------------------------------------------|
    @staticmethod
    def start(imageFrameList, nameList):
        # White Reference Path.
        for rowIdx, imgArr in enumerate(imageFrameList):
            img2 = imgArr.view(dtype=numpy.uint8)
            image = numpy.zeros((imgArr.shape[0], imgArr.shape[1], 3),
                                dtype=numpy.uint8)
            row_sampling = 1
            col_sampling = 4

            image[:, :, 2] = img2[::row_sampling, ::col_sampling]
            image[:, :, 1] = img2[::row_sampling, 1::col_sampling]
            image[:, :, 0] = img2[::row_sampling, 2::col_sampling]
            pimg = Image.fromarray(image.astype(numpy.uint8))
            name = nameList[rowIdx]
            pimg.save(name)
# |------------------------------End of start---------------------------------|


# ==============================================================================
# SoftJS
# ==============================================================================
class SoftJS(QWidget):
    '''
    Main controller class.
    '''
# |----------------------------------------------------------------------------|
# class Variables
# |----------------------------------------------------------------------------|
    ANSWER_TIME_OUT = "1000"

    BAUD_RATE = "230400"

    DELAY_BETWEEN_CHARS_MS = "0"

    HANDSHAKING = "Off"

    PARITY = "None"

    STOP_BITS = 1

# |----------------------------------------------------------------------------|
# Constructor
# |----------------------------------------------------------------------------|
    def __init__(self, parent):
        QWidget.__init__(self, parent)
        self._up = QPushButton(self)
        self._down = QPushButton(self)
        self._left = QPushButton(self)
        self._right = QPushButton(self)
        self._20x = QPushButton("20X", self)
        self._40x = QPushButton("40X", self)
        self._100x = QPushButton("100X", self)
        self._40xHighNA = QPushButton("100X(Oil)", self)
        self._Objhome = QPushButton("Home", self)
        self._focusChannel = QDoubleSpinBox(self)
        self._focusChannel.setRange(0, 2)
        self._focusChannel.setValue(1)
        self.zStackCapture = QPushButton('zStackCapture', self)
        self._whiteBalance = QCheckBox("White Balance Auto", self)
        self._pmap = None
        self._blockMetricTextVec = []
        self._checkSaturation = QCheckBox("Saturation", self)
        self._showMetric = QCheckBox("FocusMetric", self)
        self._home = QPushButton(self)
        self._zup = QPushButton(self)
        self._zdown = QPushButton(self)
        self._mmcore = MMCore.CMMCore()
        self._startZStack = QPushButton("ZStackUp", self)
        self._startZStackwithoutTrigger = QPushButton(
            "ZStackUpwithoutTrigger", self)
        self._getFusedImg = QPushButton("GetFusedImage", self)
        self._get100xStackFocus = QPushButton("Get100xStackFocus", self)
        self._zStackHeight = QSpinBox(self)
        self._zStackHeight.setRange(10, 1000)
        self._zStackHeight.setValue(5)
        self._zStackDiff = QSpinBox(self)
        self._zStackDiff.setRange(1, 10)
        self._zStackDiff.setValue(5)
        self._objRotateButton = QPushButton("Rotate Objective lens", self)
        self._setNoOfTimes = QSpinBox(self)
        self._setNoOfTimes.setRange(1, 5000)
        self.bg_focusval_threshold = 5
        self._autoFocus = autoFocus.AutoFocusInterface()
        self._imageThread = LiveThread(self._mmcore)
        self._connectBtn = QPushButton("Connect", self)
        self._propList = [("AnswerTimeout", self.ANSWER_TIME_OUT),
                          ("BaudRate", self.BAUD_RATE),
                          ("DelayBetweenCharsMs", self.DELAY_BETWEEN_CHARS_MS),
                          ("Handshaking", self.HANDSHAKING),
                          ("Parity", self.PARITY),
                          ("StopBits", self.STOP_BITS)]
        self._fMetricLabel = QLabel(":", self)
        self._XYStep = QDoubleSpinBox(self)
        self._XYStep.setRange(0, 5000)
        self._XYStep.setValue(100)
        self._XYStep.setSingleStep(6.25)
        self._ZStep = QDoubleSpinBox(self)
        self._ZStep.setRange(0, 5000)
        self._ZStep.setValue(100)
        self._ZStep.setDecimals(4)
        self._ZStep.setSingleStep(0.625)
        self._XYSpeed = QDoubleSpinBox(self)
        self._XYSpeed.setRange(0, 50)
        self._XYSpeed.setValue(10)
        self._ZSpeed = QDoubleSpinBox(self)
        self._ZSpeed.setRange(0, 10)
        self._ZSpeed.setDecimals(4)
        self._ZSpeed.setValue(0.5)
        self._xPos = QDoubleSpinBox(self)
        self._xPos.setRange(0, 1000000)
        self._yPos = QDoubleSpinBox(self)
        self._yPos.setRange(0, 1000000)
        self._zPos = QDoubleSpinBox(self)
        self._zPos.setRange(0, 1000000)
        self._zPos.setDecimals(4)
        self._readXY = QPushButton("ReadXY", self)
        self._setXY = QPushButton("SetXY", self)
        self._readZ = QPushButton("ReadZ", self)
        self._setZ = QPushButton("SetZ", self)
        self._balanceRatioDisplay = QLabel("Bal Ratio", self)
        self._balanceRatioCheck = BalanceRatioCheck(self._mmcore)
        self._zBacklashTest = QPushButton("Test Backlash", self)
        self._offset = QDoubleSpinBox(self)
        self._offset.setRange(-100, 50)
        self.msgBox = QMessageBox()
        self.msgBox.setIcon(QMessageBox.Information)
        self._disable = QPushButton("Stage", self)
        self._fld = QPushButton("FLD", self)
        self._ledPower = QPushButton("LED", self)
        self._led = QSpinBox(self)
        self._led.setRange(0, 160)
        self._led2 = QSpinBox(self)
        self._led2.setRange(0, 100)
        self._view = ZoomGraphicsView(self)
        self._scene = QGraphicsScene(self)
        self._view.setScene(self._scene)
        self._acquire = QPushButton("Acquire", self)
        self._exp = QDoubleSpinBox(self)
        self._exp.setDecimals(4)
        self._exp.setRange(0, 10000)
        self._exp.setSingleStep(0.001)
        self._exp.setValue(0.4)
        self._focus = QPushButton("Focus", self)
        self._saveImg = QPushButton("Save Image", self)
        self._colBal = QPushButton("Balance Color", self)
        self._blockMetricTextVec = []
        self._clearFocusMax(0)
        self._setupGUI()
        self._setupConnections()
        self.channelforFocus = 1
# |-------------------------End of Constructor--------------------------------|

# |----------------------------------------------------------------------------|
# closeEvent
# |----------------------------------------------------------------------------|
    def closeEvent(self, *args, **kwargs):
        try:
            self._mmcore.setProperty("XYZCom", "EnableXYZ", "OFF")
        except Exception as msg:
            print("XYZCom OFF failed: ", msg)
        return QWidget.closeEvent(self, *args, **kwargs)

# |----------------------End of closeEvent------------------------------------|

# |----------------------------------------------------------------------------|
# _setupGUI
# |----------------------------------------------------------------------------|
    def _setupGUI(self):
        self.setLayout(QGridLayout(self))
        self._up.setIcon(QIcon("Up.png"))
        self._up.setToolTip("Y -ve")
        self._down.setIcon(QIcon("Down.png"))
        self._down.setToolTip("Y +ve")
        self._left.setIcon(QIcon("Left.png"))
        self._left.setToolTip("X -ve")
        self._right.setIcon(QIcon("Right.png"))
        self._right.setToolTip("X +ve")
        self._home.setIcon(QIcon("home.png"))
        self._home.setToolTip("Home XYZ and Objective")
        self._zup.setIcon(QIcon("zup.png"))
        self._zup.setToolTip("Z +ve")
        self._zdown.setIcon(QIcon("zdown.png"))
        self._zdown.setToolTip("Z -ve")
        self._up.setFixedSize(80, 40)
        self._down.setFixedSize(80, 40)
        self._left.setFixedSize(80, 40)
        self._right.setFixedSize(80, 40)
        self._zup.setFixedSize(80, 40)
        self._zdown.setFixedSize(80, 40)
        self.layout().addWidget(self._20x, 0, 4)
        self.layout().addWidget(self._40x, 0, 5)
        self.layout().addWidget(self._100x, 0, 6)
        self.layout().addWidget(self._40xHighNA, 0, 7)
        self.layout().addWidget(self._Objhome, 0, 8)
        self.layout().addWidget(self._showMetric, 0, 9)
        self.layout().addWidget(self._fMetricLabel, 0, 10)
        self.layout().addWidget(self._checkSaturation, 0, 11)
        self.layout().addWidget(QLabel("channel for Focus:"), 0, 12)
        self.layout().addWidget(self._focusChannel, 0, 13)
        self.layout().addWidget(self._connectBtn, 0, 0)
        self.layout().addWidget(self._up, 0, 1)
        self.layout().addWidget(self._fld, 0, 2)
        self.layout().addWidget(self._ledPower, 2, 2)
        self.layout().addWidget(self._down, 2, 1)
        self.layout().addWidget(self._left, 1, 0)
        self.layout().addWidget(self._right, 1, 2)
        self.layout().addWidget(self._home, 1, 1)
        self.layout().addWidget(QLabel("XY Speed"), 3, 0)
        self.layout().addWidget(self._XYSpeed, 3, 1)
        self.layout().addWidget(self._zup, 0, 3)
        self.layout().addWidget(self._zdown, 2, 3)
        self.layout().addWidget(QLabel("Z Speed"), 3, 2)
        self.layout().addWidget(self._ZSpeed, 3, 3)
        self.layout().addWidget(QLabel("X:"), 5, 0)
        self.layout().addWidget(self._xPos, 5, 1)
        self.layout().addWidget(QLabel("Y:"), 6, 0)
        self.layout().addWidget(self._yPos, 6, 1)
        self.layout().addWidget(self._readXY, 4, 1)
        self.layout().addWidget(self._setXY, 7, 1)
        self.layout().addWidget(QLabel("Z:"), 5, 2)
        self.layout().addWidget(self._zPos, 5, 3)
        self.layout().addWidget(self._readZ, 4, 3)
        self.layout().addWidget(self._setZ, 6, 3)
        self.layout().addWidget(QLabel("LED1:"), 7, 2)
        self.layout().addWidget(self._led2, 7, 3)
        self.layout().addWidget(self._XYStep, 2, 0)
        self.layout().addWidget(self._ZStep, 1, 3)
        self.layout().addWidget(self._disable, 7, 0)
        self.layout().addWidget(self._view, 1, 4, 6, 9)
        self._view.setFixedSize(800, 800)
        self.layout().addWidget(self._acquire, 8, 4)
        self.layout().addWidget(QLabel("Exposure"), 8, 5)
        self.layout().addWidget(self._exp, 8, 6)
        self.layout().addWidget(QLabel("LED2"), 8, 7)
        self.layout().addWidget(self._led, 8, 8)
        self.layout().addWidget(self._focus, 8, 9)
        self.layout().addWidget(self._colBal, 8, 10)
        self.layout().addWidget(self._saveImg, 8, 11)
        self.layout().addWidget(self._zStackDiff, 1, 13)
        self.layout().addWidget(self._zStackHeight, 2, 13)
        self.layout().addWidget(self._startZStack, 3, 13)
        self.layout().addWidget(self._startZStackwithoutTrigger, 4, 13)
        self.layout().addWidget(self._getFusedImg, 5, 13)
        self.layout().addWidget(self._get100xStackFocus, 0, 14)
        self.layout().addWidget(QLabel("Backlash Offset: "), 6, 13)
        self.layout().addWidget(self._offset, 6, 14)
        self.layout().addWidget(self._zBacklashTest, 7, 14)
        self.layout().addWidget(self.zStackCapture, 7, 13)
        self.layout().addWidget(self._balanceRatioDisplay, 8, 13)
        self.layout().addWidget(self._whiteBalance, 8, 14)
        self.layout().addWidget(self._objRotateButton, 8, 0)
        self.layout().addWidget(self._setNoOfTimes, 8, 1)
# |--------------------------------End of _setupGUI---------------------------|

# |----------------------------------------------------------------------------|
# _setupConnections
# |----------------------------------------------------------------------------|
    def _setupConnections(self):
        self._whiteBalance.stateChanged.connect(self._balanceWhiteAuto)
        self.zStackCapture.pressed.connect(self.zStackFunc)
        self._fld.pressed.connect(self._moveFLD)
        self._ledPower.pressed.connect(self._moveLED)
        self._fld.setCheckable(True)
        self._fld.setChecked(False)
        self._ledPower.setCheckable(True)
        self._ledPower.setChecked(False)
        self._home.pressed.connect(self._goHome)
        self._connectBtn.pressed.connect(self._connectStage)
        self._left.pressed.connect(self._moveLeft)
        self._right.pressed.connect(self._moveRight)
        self._down.pressed.connect(self._moveDown)
        self._up.pressed.connect(self._moveUp)
        self._zup.pressed.connect(self._moveZUp)
        self._zdown.pressed.connect(self._moveZDown)
        self._readXY.pressed.connect(self._getXYPosition)
        self._setXY.pressed.connect(self._setXYPosition)
        self._XYSpeed.valueChanged.connect(self._setXYSpeed)
        self._ZSpeed.valueChanged.connect(self._setZSpeed)
        self._readZ.pressed.connect(self._getZPosition)
        self._setZ.pressed.connect(self._setZPositionUI)
        self._led.valueChanged.connect(self._setBrightness)
        self._led2.valueChanged.connect(self._setBarCodeLedBrightness)
        self._disable.pressed.connect(self._disableStage)
        self._disable.setCheckable(True)
        self._disable.setChecked(False)
        self._20x.pressed.connect(self._move20x)
        self._40x.pressed.connect(self._move40x)
        self._100x.pressed.connect(self._move100x)
        self._40xHighNA.pressed.connect(self._move40XHighNa)
        self._Objhome.pressed.connect(self._moveObjHome)
        self._imageThread.snappedImage.connect(self._showImage)
        self._acquire.pressed.connect(self._initCamera)
        self._exp.valueChanged.connect(self._setExposure)
        self._focusChannel.valueChanged.connect(self._setChannel)
        self._focus.pressed.connect(self._performAutoFocus)
        self._saveImg.pressed.connect(self._saveImageToDisk)
        self._colBal.pressed.connect(self._setColBal)
        self._startZStack.pressed.connect(self._captureZStack)
        self._startZStackwithoutTrigger.pressed.connect(self._captureZStack)
        self._startZStackwithoutTrigger.pressed.connect(
            self._captureZStackwithoutTrigger)
        self._getFusedImg.pressed.connect(self._getFusedImgOut)
        self._get100xStackFocus.pressed.connect(self.getFocusedImage100xStack)
        self._checkSaturation.stateChanged.connect(self._saturationCheckState)
        self._xPos.valueChanged.connect(self._clearFocusMax)
        self._yPos.valueChanged.connect(self._clearFocusMax)
        self._exp.valueChanged.connect(self._clearFocusMax)
        self._showMetric.stateChanged.connect(self._showFMetric)
        self._zBacklashTest.pressed.connect(self._backlash)
        self._objRotateButton.pressed.connect(self._rotateObjective)
# |----------------------End of _setupConnections---------------------------|


# |----------------------------------------------------------------------------|
# _rotateObjective
# |----------------------------------------------------------------------------|
    def _rotateObjective(self):
        for i in range(0, self._setNoOfTimes.value()):
            self._mmcore.setProperty("ObjectiveLens",
                                     "SetObjectiveLens", "Home")
            self._mmcore.setProperty("ObjectiveLens", "SetObjectiveLens", "4")
            print("No: ", i)
# |----------------------End of _rotateObjective---------------------------|

# |----------------------------------------------------------------------------|
# findFocusDiff
# |----------------------------------------------------------------------------|
    def findFocusDiff(self, zPos, zRef):
        self._setZPosition(zPos, True)
        img = self._snapImage()
        focusVal = self._getFocusVal(band_img=img[:, :, 1])
        focDiff = zRef - focusVal
        print(zPos, focusVal, focDiff)
        return focDiff

# |----------------------End of findFocusDiff---------------------------|

# |----------------------------------------------------------------------------|
# getFocusVal
# |----------------------------------------------------------------------------|
    def _getFocusVal(self, band_img):
        focusVal = self._autoFocus.\
            AutoFocusDriver(band_img.astype(numpy.uint8),
                            band_img.shape[1], band_img.shape[0],
                            0, 0)

        return focusVal

# |--------------------------End of getFocusVal------------------------------|

# |----------------------------------------------------------------------------|
# zStackFunc
# |----------------------------------------------------------------------------|
    def zStackFunc(self):
        bestZ = self._zPos.value()
        testDir = "/home/adminspin/zStack/"
        #testDir = "/home/adminspin/zStack/16_Aug_BloodSmear_Stacks/MonoLayer/Slide1/"
        print(bestZ)
        self._setZPosition(bestZ-50, waitTillSet=True)
        noofImages = 10
        stepSize = 0.3125
        startZ = bestZ - ((noofImages)*stepSize)-stepSize
        stopZ = bestZ + ((noofImages)*stepSize)
        count = 0
        if stopZ < 3600:
            self._setZPosition(startZ, waitTillSet=True)
            posToSet = startZ + stepSize
            print('posToSet', posToSet)
            while posToSet < stopZ:
                self._setZPosition(posToSet, True)
                img = self._snapImage()
                pimg = Image.fromarray(img.astype(numpy.uint8))
                imgName = 'img_%d.bmp' % count
                pimg.save(testDir + imgName)
#                cv2.imwrite(join(testDir, imgName), img)
                posToSet = posToSet + stepSize
                print(posToSet, count)
                count += 1
        self._setZPosition(bestZ, True)

# |----------------------End of zStackFunc---------------------------|

# |----------------------------------------------------------------------------|
# _backlash
# |----------------------------------------------------------------------------|
    def _backlash(self):
        bestZ = self._zPos.value()
        print('bestZ', bestZ)
        print(bestZ+self._offset.value())
        img = self._snapImage()
        focusVal = self._getFocusVal(band_img=img[:, :, 1])
        print('Best Focus at {} is: {}'.format(self._zPos.value(), focusVal))
        self._setZPosition(bestZ+self._offset.value(), waitTillSet=True)
        print('bestZ+offset', bestZ+self._offset.value())
        count = 0
        if (self._offset.value() < 0):
            while count < 10:
                if (abs(self.findFocusDiff(bestZ, focusVal)) < 0.5):
                    break
                bestZ = bestZ + 0.625
                count += 1
            print('backlash:', count)
            self.msgBox.setWindowTitle("Backlash")
            self.msgBox.setInformativeText('backlash: {}um'.format(
                count*0.625))
            self.msgBox.show()
        else:
            while count < 10:
                if (abs(self.findFocusDiff(bestZ, focusVal)) < 0.5):
                    break
                bestZ = bestZ - 0.625
                count += 1
            print('backlash:', count)
            self.msgBox.setWindowTitle("Backlash")
            self.msgBox.setInformativeText('backlash: {}um'.format(
                count*0.625))
            self.msgBox.show()
        self._zPos.setValue(bestZ)
# |----------------------End of _backlash---------------------------|

# |----------------------------------------------------------------------------|
# _showFMetric
# |----------------------------------------------------------------------------|
    def _showFMetric(self, state):
        if self._showMetric.checkState() == 0:
            self._imageThread._metric = False
        if self._showMetric.checkState() == 2:
            self._imageThread._metric = True
# |----------------------End of _showF---------------------------|

# |----------------------------------------------------------------------------|
# _clearFocusMax
# |----------------------------------------------------------------------------|
    def _clearFocusMax(self, val):
        self._focusMaxVec = [0.0]*17
# |----------------------End of _clearFocusMax---------------------------|


# |----------------------------------------------------------------------------|
# _connectStage
# |----------------------------------------------------------------------------|
    def _connectStage(self):
        self._mmcore.unloadAllDevices()
        retVal = False
        portNameList = self._mmcore.getAvailableDevices("SerialManager")
        for comPort in portNameList:
            print("Trying COM Port: " + str(comPort))
            try:
                self._initComPort(comPort, self._propList)
                propInfo = ("Port", comPort)
                propList = []
                propList.append(propInfo)
                self._initDevice("Scope", "VulcanScopeDDMod", propList)
                self._initDevice("XYStage", "VulcanScopeDDMod")
                self._initDevice("ZStage", "VulcanScopeDDMod")
                self._initDevice("XYZCom", "VulcanScopeDDMod")
                self._initDevice("WhiteLamp", "VulcanScopeDDMod")
                self._initDevice("JoyStick", "VulcanScopeDDMod")
                print(self._mmcore.getAvailableDevices("VulcanScopeDDMod"))
                self._initDevice("ObjectiveLens", "VulcanScopeDDMod")
                self._mmcore.setProperty("XYZCom", "EnableXYZ", "ON")
                self._mmcore.setProperty("XYZCom", "SetLimitX", 200000)
                self._mmcore.setProperty("XYZCom", "SetLimitY", 200000)
                self._mmcore.setProperty("XYZCom", "SetLimitZ", 232000)
                self._mmcore.setProperty("XYZCom", "EnableSetLimitXYZ", "ON")
                self._mmcore.setProperty("XYZCom", "MicroStepX", 8)
                self._mmcore.setProperty("XYZCom", "MicroStepY", 8)
                self.zMicroStepping = 16
                self._mmcore.setProperty("XYZCom", "MicroStepZ",
                                         self.zMicroStepping)
                self._mmcore.setProperty("XYZCom", "MicroStepTurret", 8)
                self._mmcore.setProperty("XYZCom", "XYRunCurrent", 12)
                self._mmcore.setProperty("XYZCom", "ZRunCurrent", 15)
                self._mmcore.setProperty("XYZCom", "TurretRunCurrent", 12)
                self._mmcore.setProperty("XYZCom", "XYHoldCurrent", 5)
                self._mmcore.setProperty("XYZCom", "ZHoldCurrent", 5)
                self._mmcore.setProperty("XYZCom", "TurretHoldCurrent", 5)
                self._mmcore.setProperty("XYZCom", "XStageMode", 3)
                self._mmcore.setProperty("XYZCom", "YStageMode", 3)
                self._mmcore.setProperty("XYZCom", "ZStageMode", 3)
                self._mmcore.setProperty("XYZCom", "TurretMode", 3)
                self._mmcore.setProperty("XYZCom", "EnableCurrentSettingZ", "ON")
                self._mmcore.setProperty("XYZCom", "EnableCurrentSettingXY", "ON")
                self._mmcore.setProperty("XYZCom", "EnableCurrentSettingTurret", "ON")
                self._mmcore.setProperty("XYZCom", "EnableMicroStepXYZ", "ON")
                self._mmcore.setProperty("ZStage", "BacklashZ", 0)
                print(self._mmcore.getProperty("ZStage", "BacklashZ"))
                retVal = True
                break
            except Exception as msg:
                retVal = False
                print("Exception occurred in "
                      "AutoConfigureLabEndScope::initStage: ", msg)
        self._mmcore.setProperty("XYStage", "SpeedXY", 10)
        self._mmcore.setProperty("ZStage", "SpeedZ", 0.5)
        self._xPos.setValue(self._mmcore.getXPosition("XYStage"))
        self._yPos.setValue(self._mmcore.getYPosition("XYStage"))
        self._zPos.setValue(self._mmcore.getPosition("ZStage"))
        print("Connected")
        print("White Lamp Props: ",
              self._mmcore.getDevicePropertyNames("WhiteLamp"))
        print("Prop Vals: ", self._mmcore.getAllowedPropertyValues(
            "WhiteLamp",
            "SetLedPower"))
        stepSize = float(self._mmcore.getProperty("XYStage",
                                                  "GetStepSizeXYUm"))
        print('micronPerSteps', stepSize)
        print(retVal)
        return retVal
# |---------------------------------End of _connectStage----------------------|
# |----------------------------------------------------------------------------|

# _showBalanceRatio
# |----------------------------------------------------------------------------|
    def _showBalanceRatio(self, text):
        self._balanceRatioDisplay.setText(text)

# |----------------------End of _show---------------------------|

# |----------------------------------------------------------------------------|
# _initDevice
# |----------------------------------------------------------------------------|
    def _initDevice(self, devName, libName, propList=None):
        # First load the device.
        self._mmcore.loadDevice(devName, libName, devName)

        # Set the properties if any.
        if propList is not None:
            for propInfo in propList:
                propName = propInfo[0]
                propVal = propInfo[1]

                # Doing try catch because some properties might not be
                # modifiable.
                try:
                    self._mmcore.setProperty(devName, propName, propVal)
                except Exception:
                    pass

        # Initialize device.
        try:
            self._mmcore.initializeDevice(devName)
            if "TOFRA" in devName or "ZStage" in devName:
                self._mmcore.setFocusDevice(devName)
            return True, ""
        except Exception as msg:
            return False, msg
# |----------------------End of _initDevice---------------------------|

# |----------------------------------------------------------------------------|
# _initComPort
# |----------------------------------------------------------------------------|
    def _initComPort(self, comPortName, comPortPropList):
        # Load and initialize COM port.
        self._mmcore.loadDevice(comPortName, "SerialManager", comPortName)
        try:
            for propInfo in comPortPropList:
                propName = propInfo[0]
                propVal = propInfo[1]
                # Doing try catch because some properties might not be
                # modifiable.
                try:
                    self._mmcore.setProperty(comPortName, propName, propVal)
                except Exception as msg:
                    print("Exception occurred in "
                          "HardwareManager::initComPort: ", msg)

            self._mmcore.initializeDevice(comPortName)

            return True, ""
        except Exception as msg:
            return False, msg

# |----------------------------------------------------------------------------|
# _initCamera
# |----------------------------------------------------------------------------|
    def _initCamera(self):
        retVal = False
        adapterName = "MatrixVision"
        devNameList = self._mmcore.getAvailableDevices(adapterName)
        for devName in devNameList:
            if "FF" in devName:
                print("Loading device: ", devName)
                try:
                    self._initDevice(devName, adapterName)
                    self._mmcore.setCameraDevice(devName)
                    self._mmcore.setProperty(devName,
                                             "Camera/GenICam/" +
                                             "AcquisitionControl/" +
                                             "TriggerMode", "Off")
                    self._mmcore.setProperty(devName,
                                             "Camera/GenICam/" +
                                             "AcquisitionControl/"
                                             "ExposureAuto", "Off")
                    self._mmcore.setProperty(devName,
                                             "Camera/GenICam/" +
                                             "AcquisitionControl/"
                                             "AcquisitionMode", "SingleFrame")
                    self._mmcore.setProperty(devName,
                                             "Camera/GenICam/AnalogControl/"
                                             "GainAuto", "Off")
                    propName = "ImageProcessing/ColorTwist/ColorTwistEnable"
                    self._mmcore.setProperty(devName, propName, "Off")

                    propName = "ImageProcessing/ColorTwist/" +\
                               "ColorTwistInputCorrectionMatrixEnable"
                    self._mmcore.setProperty(devName, propName, "Off")

                    propName = "ImageProcessing/ColorTwist/" +\
                               "ColorTwistOutputCorrectionMatrixEnable"
                    self._mmcore.setProperty(devName, propName, "Off")
                    propName = "ImageProcessing/ColorTwist/" +\
                               "ColorTwistOutputCorrectionMatrixMode"
                    self._mmcore.setProperty(devName, propName,
                                             "XYZTosRGB_D50")
                    self._snapImage()
                    propName = "Camera/GenICam/AnalogControl/BalanceWhiteAuto"
                    self._mmcore.setProperty(devName, propName, "Off")
                    propName = "Camera/GenICam/AnalogControl/" +\
                               "BalanceRatioSelector"
                    self._mmcore.setProperty(devName, propName, "Red")
                    # 1.960
                    # 2.06
                    # 2.024
                    # on feb 28: 2.099 #new 2.081
                    # on march 13: changed from 1.969 to 1.937
                    propName = "Camera/GenICam/AnalogControl/BalanceRatio"
                    # self._mmcore.setProperty(devName, propName, 1.798)
                    ##self._mmcore.setProperty(devName, propName, 1.8828)
                    self._mmcore.setProperty(devName, propName, 2.1026)

                    propName = "Camera/GenICam/AnalogControl/" +\
                               "BalanceRatioSelector"
                    self._mmcore.setProperty(devName, propName, "Blue")

                    # 1.936
                    # 1.98
                    # 2.009
                    # on march 13: changed from 1.730 to 1.767
                    propName = "Camera/GenICam/AnalogControl/BalanceRatio"
                    # self._mmcore.setProperty(devName, propName, 2.806)
                    ##self._mmcore.setProperty(devName, propName, 1.7199)
                    self._mmcore.setProperty(devName, propName, 1.4444)
                    propName = "ImageProcessing/LUTOperations/LUTEnable"
                    self._mmcore.setProperty(devName, propName, "Off")
                    self._mmcore.setExposure(0.4)
                    propName = "Camera/GenICam/ImageFormatControl/PixelFormat"
                    print(self._mmcore.getAllowedPropertyValues(devName,
                                                                propName))
                    self._balanceRatioDisplay.setText(str(1.9643)+"/" +
                                                      str(1.7156))
                    self._cameraName = devName
                    self._balanceRatioCheck._cameraName = devName
                    self._balanceRatioCheck.balanceRatioText.connect(
                        self._showBalanceRatio)
                    retVal = True
                    break
                except Exception as msg:
                    retVal = False
                    print("Exception occurred in "
                          "AutoConfigureLabEndScope::initStage: ", msg)
        self._imageThread.start()
        self._showGridLines()
        return retVal
# |----------------------------End of _initCamera-----------------------------|

# |----------------------------------------------------------------------------|
# _getFusedImgOut
# |----------------------------------------------------------------------------|
    def _getFusedImgOut(self):
        stepsize = 1
        p = self._mmcore.getPosition("ZStage")
        totalCount = 3

        self._setZPosition(p-stepsize, waitTillSet=True)
        print('current Z location', self._mmcore.getPosition("ZStage"))

        count = 0
        imgstack = []
        while (count < totalCount):
            p = self._setFocalVal(stepsize)
            resizedimg = self._snapImage()
            resizedimg = cv2.cvtColor(resizedimg, cv2.COLOR_BGR2RGB)
            imgstack.append(resizedimg)
            count = count+1

        print("FLD state = ", self._mmcore.getProperty("XYZCom", "SetFilter"))
        if self._mmcore.getProperty("XYZCom", "SetFilter") == "ON":
            self._mmcore.setProperty("XYZCom", "SetFilter", "OFF")
        # Call fusing function
        # Write images to be fused to disk
  #      fusionUtil = FusionUtil()
#         fusionUtil.writeImageStack(imgstack)
#
#         if fusionUtil.instantiate() is True:
#             fusionUtil.processStack()

#         outImg = cv2.imread(fusionUtil._fusedImagePath)

        # fused = fusion(imgstack)
        # cv2.imwrite(str(time.time())+"_distorted.bmp", fused)
        # put the filter and capture the distorted image

        p = self._setFocalVal(-stepsize)

        # Toggle FLD
        if self._mmcore.getProperty("XYZCom", "SetFilter") == "ON":
            self._mmcore.setProperty("XYZCom", "SetFilter", "OFF")
        else:
            self._mmcore.setProperty("XYZCom", "SetFilter", "ON")

        print("FLD state = ", self._mmcore.getProperty("XYZCom", "SetFilter"))

        resizedimg = self._snapImage()
        resizedimg = cv2.cvtColor(resizedimg, cv2.COLOR_BGR2RGB)

        cv2.imwrite(join(fusionUtil._fusedImageDir,
                         str(time.time())+"_distorted.bmp"), resizedimg)

        self._mmcore.setProperty("XYZCom", "SetFilter", "OFF")
# |---------------------------------End of _getFusedImgOut--------------------|


# |----------------------------------------------------------------------------|
# _balanceWhiteAuto
# |----------------------------------------------------------------------------|
    def _balanceWhiteAuto(self):
        if self._whiteBalance.isChecked():
            propName = "Camera/GenICam/AnalogControl/BalanceWhiteAuto"
            self._mmcore.setProperty(self._cameraName, propName, "Continuous")
            self._balanceRatioCheck.start()
        else:
            self._balanceRatioCheck.exit()
            propName = "Camera/GenICam/AnalogControl/BalanceWhiteAuto"
            self._mmcore.setProperty(self._cameraName, propName, "Off")
# |----------------------End of _balanceWhiteAuto---------------------------|

# |----------------------------------------------------------------------------|
# _captureZStackwithoutTrigger
# |----------------------------------------------------------------------------|
    def _captureZStackwithoutTrigger(self):
        pass
# |----------------------End of _captureZStackwithoutTrigger------------------|

# |----------------------------------------------------------------------------|
# _captureZStack
# |----------------------------------------------------------------------------|
    def _captureZStack(self):
        self._imageThread.exit()
        self._mmcore.setProperty(self._camDev,
                                 "Camera/GenICam/AcquisitionControl/"
                                 "AcquisitionMode", "Continuous")
        # Doing a dummy snap.
        self._snapImage()

        self._mmcore.setProperty(self._camDev, "Camera/GenICam/" +
                                 "AcquisitionControl/TriggerMode", "On")

        self._mmcore.setProperty(self._camDev, "Camera/GenICam/" +
                                 "AcquisitionControl/TriggerSelector",
                                 "FrameStart")

        self._mmcore.setProperty(self._camDev, "Camera/GenICam/" +
                                 "AcquisitionControl/TriggerSource",
                                 "Line4")

        self._mmcore.setProperty(self._camDev, "Camera/GenICam/" +
                                 "AcquisitionControl/TriggerActivation",
                                 "RisingEdge")
        self._snapThread = QThread()
        self._snapWorker = SnapThread()
        self._snapWorker.moveToThread(self._snapThread)

        # When the thread starts, start the worker process.
        self._snapThread.started.connect(self._snapWorker.onStartProcess)

        self._mmcore.setProperty("XYStage",
                                 "XTrigger",
                                 self._zStackDiff.value()*8)

        self._mmcore.setProperty("XYStage",
                                 "EnableTrigger",
                                 "ON")
        # Process the image that was snapped during stage movement.
        self._snapWorker.capturedImgArr.connect(self._zStackImageCaptured)
        hv = self._zStackHeight.value()
        dv = self._zStackDiff.value()
        self._snapWorker._imageCount = hv/dv
        # Process the acquired stack.
        self._snapWorker.processFinished.connect(self._zStackAcquired)
        self._snapThread.start()
        currentZPos = self._mmcore.getPosition("ZStage")
        zh = self._zStackHeight.value()
        self._mmcore.setPosition("ZStage", currentZPos + zh)
# |----------------------------End of _captureZStack--------------------------|

# |----------------------------------------------------------------------------|
# _zStackImageCaptured
# |----------------------------------------------------------------------------|
    def _zStackImageCaptured(self, imgArr):
        self._imgFrameList.append(imgArr)
# |---------------------End of _zStackImageCaptured---------------------------|

# |----------------------------------------------------------------------------|
# _zStackAcquired
# |----------------------------------------------------------------------------|
    def _zStackAcquired(self):
        self._mmcore.setProperty(self._camDev, "Camera/GenICam/" +
                                 "AcquisitionControl/TriggerMode", "Off")
        self._mmcore.setProperty(self._camDev,
                                 "Camera/GenICam/AcquisitionControl/"
                                 "AcquisitionMode", "SingleFrame")
        imc = self._snapWorker._imageCount
        nameList = [str("{0:03d}.bmp".format(n)) for n in range(imc)]
        imageNameList = nameList
        p = Process(target=WriteBufferToDisk.start, args=(self._imgFrameList,
                                                          imageNameList))
        p.start()
        self._imageThread.start()
# |----------------------End of _zStackAcquired---------------------------|

# |----------------------------------------------------------------------------|
# _saturationCheckState
# |----------------------------------------------------------------------------|
    def _saturationCheckState(self):
        if self._checkSaturation.checkState() == 2:
            self._imageThread._saturation = True
        if self._checkSaturation.checkState() == 0:
            self._imageThread._saturation = False
# |----------------------End of _saturationCheckState-------------------------|

# |----------------------------------------------------------------------------|
# _setZPosition
# |----------------------------------------------------------------------------|
    def _setZPosition(self, position, waitTillSet=False):
        print(type(position))
        self._mmcore.setPosition(position)
        if waitTillSet:
            while self._mmcore.deviceBusy("ZStage"):
                time.sleep(0.1)
# |----------------------End of _setZPosition---------------------------------|

# |----------------------------------------------------------------------------|
# _goHome
# |----------------------------------------------------------------------------|
    def _goHome(self):
        self._mmcore.home("ZStage")
        self._mmcore.setProperty("ObjectiveLens",
                                 "SetObjectiveLens", "Home")
        self._mmcore.home("XYStage")

        self._xPos.setValue(0)
        self._yPos.setValue(0)
        self._zPos.setValue(0)
# |----------------------End of _goHome---------------------------------------|

# |----------------------------------------------------------------------------|
# _setBarCodeLedBrightness
# |----------------------------------------------------------------------------|
    def _setBarCodeLedBrightness(self, value):
        self._mmcore.setProperty("WhiteLamp", "SetOneXLedBrightness", value)
# |---------------------------End of _setBarCodeLedBrightness-----------------|

# |----------------------------------------------------------------------------|
# _setBrightness
# |----------------------------------------------------------------------------|
    def _setBrightness(self, value):
        self._mmcore.setProperty("WhiteLamp", "SetLedBrightness", value)
# |---------------------------End of _setBrightness---------------------------|

# |----------------------------------------------------------------------------|
# _getXYPosition
# |----------------------------------------------------------------------------|
    def _getXYPosition(self):
        self._xPos.setValue(self._mmcore.getXPosition("XYStage"))
        self._yPos.setValue(self._mmcore.getYPosition("XYStage"))
# |----------------------End of _getXYPosition--------------------------------|

# |----------------------------------------------------------------------------|
# _setXYPosition
# |----------------------------------------------------------------------------|
    def _setXYPosition(self):
        self._mmcore.setXYPosition("XYStage", self._xPos.value(),
                                   self._yPos.value())
# |----------------------End of _setXYPosition--------------------------------|

# |----------------------------------------------------------------------------|
# _moveLeft
# |----------------------------------------------------------------------------|
    def _moveLeft(self):
        while self._mmcore.deviceBusy("XYStage"):
            time.sleep(0.1)
        xPos = self._mmcore.getXPosition("XYStage")
        yPos = self._mmcore.getYPosition("XYStage")
        self._xPos.setValue(xPos)
        if xPos >= self._XYStep.value():
            print(xPos-self._XYStep.value(), yPos)
            self._mmcore.setXYPosition("XYStage",
                                       xPos-self._XYStep.value(), yPos)
            self._xPos.setValue(xPos-self._XYStep.value())
# |--------------------------------End of _moveLeft---------------------------|

# |----------------------------------------------------------------------------|
# _moveRight
# |----------------------------------------------------------------------------|
    def _moveRight(self):
        while self._mmcore.deviceBusy("XYStage"):
            time.sleep(0.1)
        xPos = self._mmcore.getXPosition("XYStage")
        yPos = self._mmcore.getYPosition("XYStage")
        self._xPos.setValue(xPos)
        print(xPos+self._XYStep.value(), yPos)
        self._mmcore.setXYPosition("XYStage", xPos+self._XYStep.value(), yPos)
        self._xPos.setValue(xPos+self._XYStep.value())
# |--------------------------------End of _moveRight--------------------------|

# |----------------------------------------------------------------------------|
# _moveUp
# |----------------------------------------------------------------------------|
    def _moveUp(self):
        while self._mmcore.deviceBusy("XYStage"):
            time.sleep(0.1)
        xPos = self._mmcore.getXPosition("XYStage")
        yPos = self._mmcore.getYPosition("XYStage")
        self._yPos.setValue(yPos)
        print(xPos, yPos-self._XYStep.value())
        if yPos >= self._XYStep.value():
            self._mmcore.setXYPosition("XYStage", xPos,
                                       yPos-self._XYStep.value())
            self._yPos.setValue(yPos-self._XYStep.value())
# |--------------------------------End of _moveUp---------------------------|

# |----------------------------------------------------------------------------|
# _moveDown
# |----------------------------------------------------------------------------|
    def _moveDown(self):
        while self._mmcore.deviceBusy("XYStage"):
            time.sleep(0.1)
        xPos = self._mmcore.getXPosition("XYStage")
        yPos = self._mmcore.getYPosition("XYStage")
        self._yPos.setValue(yPos)
        print(xPos, yPos+self._XYStep.value())
        self._mmcore.setXYPosition("XYStage", xPos, yPos+self._XYStep.value())
        self._yPos.setValue(yPos+self._XYStep.value())
# |--------------------------------End of _moveDown---------------------------|

# |----------------------------------------------------------------------------|
# _moveZDown
# |----------------------------------------------------------------------------|
    def _moveZDown(self):
        while self._mmcore.deviceBusy("ZStage"):
            time.sleep(0.2)
        zPos = self._mmcore.getPosition("ZStage")
        time.sleep(0.1)
        if zPos - self._ZStep.value() >= 0:
            self._mmcore.setPosition("ZStage", zPos-self._ZStep.value())
        self._zPos.setValue(zPos-self._ZStep.value())
        time.sleep(0.2)
        while self._mmcore.deviceBusy("ZStage"):
            time.sleep(0.2)
# |--------------------------------End of _moveZDown--------------------------|

# |----------------------------------------------------------------------------|
# _moveZUp
# |----------------------------------------------------------------------------|
    def _moveZUp(self):
        while self._mmcore.deviceBusy("ZStage"):
            time.sleep(0.2)
        zPos = self._mmcore.getPosition("ZStage")
        time.sleep(0.1)
        if zPos + self._ZStep.value() <= 3600:
            print(zPos + self._ZStep.value())
            print('entered')
            self._mmcore.setPosition("ZStage", zPos+self._ZStep.value())
            self._zPos.setValue(zPos+self._ZStep.value())
        else:
            self.msgBox.setWindowTitle("zLimit warning!")
            self.msgBox.setInformativeText(
                'zLimit reached please check the zValue')
            self.msgBox.show()
        time.sleep(0.2)
        while self._mmcore.deviceBusy("ZStage"):
            time.sleep(0.2)
# |--------------------------------End of _moveZUp----------------------------|

# |----------------------------------------------------------------------------|
# _getZPosition
# |----------------------------------------------------------------------------|
    def _getZPosition(self):
        zPos = self._mmcore.getPosition("ZStage")
        self._zPos.setValue(zPos)
        return zPos
# |----------------------End of _getZPosition---------------------------|

# |----------------------------------------------------------------------------|
# _setZPositionUI
# |----------------------------------------------------------------------------|
    def _setZPositionUI(self, waitTillSet=True):
        # if self._mmcore.getPosition("ZStage") - self._zPos.value() < 0:
        if self._zPos.value() <= 3600:
            self._mmcore.setPosition("ZStage", self._zPos.value())
            while waitTillSet and self._mmcore.deviceBusy("ZStage"):
                time.sleep(0.1)
        else:
            self.msgBox.setWindowTitle("zLimit warning!")
            self.msgBox.setInformativeText(
                'zLimit reached please check the zValue')
            self.msgBox.show()
# |----------------------End of _setZPositionUI---------------------------|

# |----------------------------------------------------------------------------|
# _setXYSpeed
# |----------------------------------------------------------------------------|
    def _setXYSpeed(self, speed):
        self._mmcore.setProperty("XYStage", "SpeedXY", speed)
# |--------------------------------End of _setXYSpeed-------------------------|

# |----------------------------------------------------------------------------|
# _moveFLD
# |----------------------------------------------------------------------------|
    def _moveFLD(self):
        if self._fld.isChecked():
            print("FLD ON")
            self._fld.setText("FLD ON")
            self._mmcore.setProperty("XYZCom", "SetFilter", "ON")
        else:
            print("FLD OFF")
            self._fld.setText("FLD OFF")
            self._mmcore.setProperty("XYZCom", "SetFilter", "OFF")
# |----------------------End of moveFLD---------------------------|

# |----------------------------------------------------------------------------|
# _moveFLD
# |----------------------------------------------------------------------------|
    def _moveLED(self):
        if self._ledPower.isChecked():
            self._ledPower.setText("LED ON")
            self._mmcore.setProperty("WhiteLamp", "SetLedPower", "ON")
        else:
            self._ledPower.setText("LED OFF")
            self._mmcore.setProperty("WhiteLamp", "SetLedPower", "OFF")
# |----------------------End of moveFLD---------------------------|

# |----------------------------------------------------------------------------|
# _setZSpeed
# |----------------------------------------------------------------------------|
    def _setZSpeed(self, speed):
        self._mmcore.setProperty("ZStage", "SpeedZ", speed)
# |--------------------------------End of _setZSpeed--------------------------|

# |----------------------------------------------------------------------------|
# _disableStage
# |----------------------------------------------------------------------------|
    def _disableStage(self):
        if self._disable.isChecked():
            self._disable.setText("Enable")
            self._mmcore.setProperty("XYZCom", "EnableXYZ", "OFF")
        else:
            self._disable.setText("Disable")
            self._mmcore.setRelativePosition("ZStage", 0)
            self._mmcore.setRelativeXYPosition("XYStage", 0, 0)
            self._mmcore.getPosition("ZStage")
            self._mmcore.setProperty("XYZCom", "EnableXYZ", "ON")
# |----------------------End of _disableStage---------------------------|

# |----------------------------------------------------------------------------|
# _move20x
# |----------------------------------------------------------------------------|
    def _move20x(self):
        while self._mmcore.deviceBusy("XYStage"):
            time.sleep(0.1)
        self._mmcore.setProperty("ObjectiveLens", "SetObjectiveLens", "1")
# |----------------------End of _move20x--------------------------------------|

# |----------------------------------------------------------------------------|
# _move40x
# |----------------------------------------------------------------------------|
    def _move40x(self):
        while self._mmcore.deviceBusy("XYStage"):
            time.sleep(0.1)
        self._mmcore.setProperty("ObjectiveLens", "SetObjectiveLens", "2")
# |----------------------End of _move40x--------------------------------------|

# |----------------------------------------------------------------------------|
# _move100x
# |----------------------------------------------------------------------------|
    def _move100x(self):
        while self._mmcore.deviceBusy("XYStage"):
            time.sleep(0.1)
        self._mmcore.setProperty("ObjectiveLens", "SetObjectiveLens", "3")
# |----------------------End of _move100x-------------------------------------|

# |----------------------------------------------------------------------------|
# _move100x
# |----------------------------------------------------------------------------|
    def _move40XHighNa(self):
        while self._mmcore.deviceBusy("XYStage"):
            time.sleep(0.1)
        self._mmcore.setProperty("ObjectiveLens", "SetObjectiveLens", "4")
# |----------------------End of _move100x-------------------------------------|

# |----------------------------------------------------------------------------|
# _moveObjHome
# |----------------------------------------------------------------------------|
    def _moveObjHome(self):
        while self._mmcore.deviceBusy("XYStage"):
            time.sleep(0.1)
        self._mmcore.setProperty("ObjectiveLens", "SetObjectiveLens", "Home")
# |----------------------End of _moveObjHome----------------------------------|

# |----------------------------------------------------------------------------|
# _performAutoFocus
# |----------------------------------------------------------------------------|
    def _performAutoFocus(self):
        self._imageThread.exit()
        typical_z = self._mmcore.getPosition("ZStage")
        self.performExhaustAutoFocusNew(typical_z)
        self._imageThread.start()
        print("Image Thread Started")
# |----------------------End of _performAutoFocus---------------------------|

# |----------------------------------------------------------------------------|
# performExhaustAutofocusNew
# |----------------------------------------------------------------------------|
    def performExhaustAutoFocusNew(self, typical_z):
        print('Channel used ', self.channelforFocus)
        print('performExhaustAutoFocusNew with typical_z', typical_z)
#         startZVal = typical_z - 5
        startZVal = typical_z
        self._setZPosition(startZVal, waitTillSet=False)
        time.sleep(0.02)
        print('current Z location', self._mmcore.getPosition("ZStage"))
        direction = 1
        stepsize = 1.5 * direction
        maxZVal = 28500
        # Set input parameters
#         stepsize = 20 * dir
        yOffset = 32
        write_img = 0
        maxZReached = False
        focusVal = []
        maxFocusVal = 0
        maxFocusLocation = 0
        previousFocusVal = 0

        # 1.)Get the image from the Camera
        out_image = self._snapImage()
        resizedimg = self._getBandImage(out_image, yOffset, 1)

        # 3.) Get the focus value from different algorithms
        curfocusVal = self._getFocalVal(resizedimg, 0, write_img)

        # For BG detection.
        if(curfocusVal < self.bg_focusval_threshold):

            try:
                # To confirm if the input image is BG image
                bg_val = self._confirmBackground(out_image)
#                 bg_val, focus_val = self.ifImageFocused(out_image)
                print('bg_val, focus_val', bg_val)
            except Exception as msg:
                print('Exhaustive autofocus:  Running confirm BG', msg)
            if(bg_val == 1):
                return 2

        stepCount = 0
        while maxZReached is False:
            p = self._mmcore.getPosition("ZStage")

            # Check if the z value reached to max Z value possible
            if(p >= maxZVal):
                # maxZReached =  True
                break

            # Set Z value in steps
            p = self._setFocalVal(stepsize)
            resizedimg = self._snapImage()
            resizedimg = self._getBandImage(resizedimg, yOffset, 1)
            # 3.) Get the focus value from different algorithms
            curfocusVal = self._getFocalVal(resizedimg, 0, write_img)
            print('Exhaustive autofocus focusVal', curfocusVal, p)
            # 4.) Push the focus Val into vector
            focusVal.append([p, curfocusVal])
            # Get the maximum focus value
            if (curfocusVal > maxFocusVal):
                maxFocusVal = curfocusVal
                maxFocusLocation = p

            # Check if the curFocusVal is less than previousFocus Val
            # if some local maxima
            if (curfocusVal < previousFocusVal):
                p = self._setFocalVal(2*stepsize)
                resizedimg = self._snapImage()
                resizedimg = self._getBandImage(resizedimg, yOffset, 1)
                # 3.) Get the focus value from different algorithms
                curfocusValtemp = self._getFocalVal(resizedimg, 0, write_img)
                if (curfocusValtemp < curfocusVal):
                    # have to go in reverse direction
                    break
                else:
                    p = self._setFocalVal(-1*stepsize)
            stepCount = stepCount + 1
            previousFocusVal = curfocusVal

        self._setZPosition(maxFocusLocation, waitTillSet=False)
        time.sleep(0.02)
        print('set location: maxFocusLocation', maxFocusLocation)
        in_img = self._snapImage()
        resizedimg = self._getBandImage(in_img, yOffset, 1)
        curfocusVal = self._getFocalVal(resizedimg, 0, write_img)

        # For BG detection.
        if(curfocusVal < self.bg_focusval_threshold):

            try:
                # To confirm if the input image is BG image
                bg_val = self._confirmBackground(in_img)
#                 bg_val, focus_val = self.ifImageFocused(out_image)
            except Exception as msg:
                print('getBestFocusSlide:  Running confirm BG', msg)
            # Return the particular location is BG
            if(bg_val == 1):
                return 2
        if stepCount == 1:
            if(curfocusValtemp < curfocusVal):
                print('Checking reverse direction')
                result = self.checkReverseDirection(startZVal-5)
                return 1
        else:
            print('No Reverse direction')
        # Return the current location is FG and best focus val
            return 1

#             prevfocusVal = curfocusVal
        return 2
# |----------------------End of performExhaustAutofocusNew--------------------|

    def checkReverseDirection(self, typical_z_val):
        result = self.performExhaustAutoFocusNew(typical_z_val)
        return result

# |----------------------------------------------------------------------------|
# createZStack
# |----------------------------------------------------------------------------|
    def createZStack(self, startZ, stopZ, stepSize):
        startZ = startZ
        stopZ = stopZ

        backlash_offset = 0

        if stopZ > startZ:
            stepSize = stepSize
        # To move in the reverse direction
        else:
            stepSize = -1 * stepSize
            # Add the backlash to start position to go
            # to correct location

        self._setZPosition(startZ, waitTillSet=True)

        maxZReached = False
        imageStack = []
        focusval = []
        # Capturing the images in hardware trigger mode

        # for  curZ in range(startZ, stopZ, stepSize):
        while maxZReached is False:
            currZ = self._getZPosition()
            # Check if the z value reached to stop Z value possible
            if stepSize > 0:
                if(currZ >= stopZ):
                    maxZReached = True
                    print('stop reached')
                    break
            if stepSize < 0:
                if(currZ <= stopZ):
                    maxZReached = True
                    print('stop reached')
                    break
            print('current Z', currZ)
            # Get the current Image
            resizedimg = self._snapImage()
            imageStack.append(resizedimg)
            yOffset = 32
            resizedimg = self._getBandImage(resizedimg, yOffset, 1)
            # 3.) Get the focus value from different algorithms
            focusMetric = self._getFocalVal(resizedimg, 0, 0)
            print('focusMetric', focusMetric)
            # focusval.append(focusMetric)
            # Set Z value in steps
            currZ = self._setFocalVal(stepSize)

        self._setZPosition(startZ, waitTillSet=True)

        return imageStack

# |----------------------------------------------------------------------------|
# getBestFocusFrmStack
# |----------------------------------------------------------------------------|
    def getBestFocusImgFrmStack(self, stackImg):

        bestmetric = -100
        bestcount = -10

        for count, inImage in enumerate(stackImg):
            yOffset = 32
            inImage = self._getBandImage(inImage, yOffset, 1)
            metricSamplingby2 = self._getFocalVal(inImage[::2, ::2], 0, 0)
            metricSamplingby3 = self._getFocalVal(inImage[::3, ::3], 0, 0)
            metric = self._getFocalVal(inImage, 0, 0)
            print(
                "Exhaustive autofocus focusVal" +
                str(metric)
                + "img Count" + str(count) +
                '  metricsamplingby2 '+str(metricSamplingby2)
                + ' metricsamplingby3 ' + str(metricSamplingby3))
            if metric > bestmetric:
                bestmetric = metric
                bestcount = count

        print('bestmetric', bestmetric)
        print('bestIdx', bestcount)

        bestActual = bestcount + 3
        print('bestIdx', bestActual)

        if bestcount is not -10:
            resultIm = stackImg[bestActual]
            bestIdx = bestActual

        return resultIm, bestIdx

# |--------getBestFocusImgFrmStack--------------------------------------------|

# |----------------------------------------------------------------------------|
# getFocusedImage
# |----------------------------------------------------------------------------|
    def getFocusedImage100xStack(self):

        startZ = self._getZPosition()
        self._setZPosition(startZ-20, waitTillSet=True)
        print('startZ', startZ)
        stopZ = startZ + 10
        stepSize = 0.625
        print("startZ, stopZ, steps", startZ, stopZ, stepSize)
        imgStack = self.createZStack(startZ, stopZ, stepSize)

        FUSESTACK = False

        bestZ = (startZ + stopZ)/2

        if FUSESTACK is True:
            # Write images to be fused to disk
            self._fusionUtil.writeImageStack(imgStack)

#             if self._fusionUtil.instantiate() is True:
#                 self._fusionUtil.processStack()

#             outImg = cv2.imread(self._fusionUtil._fusedImagePath)
            outImg = self.fuseStack(imgStack)
        else:
            # self._fusionUtil.writeImageStack(imgStack)
            outImg, bestIndx = self.getBestFocusImgFrmStack(imgStack)
        # Best Z value
        bestZ = (startZ + stopZ)/2
        outImg1 = numpy.zeros(outImg.shape)

        outImg1[:, :, 2] = outImg[:, :, 0]
        outImg1[:, :, 1] = outImg[:, :, 1]
        outImg1[:, :, 0] = outImg[:, :, 2]

        cv2.imwrite(str(time.time())+"_100xStackOut.bmp", outImg1)

        self._setZPosition(startZ - 10, waitTillSet=True)
        setbestZ = startZ + bestIndx*stepSize

        print('setbestZ', setbestZ)
        self._setZPosition(setbestZ, waitTillSet=True)

        return outImg, setbestZ

# |----------------------------------------------------------------------------|
# _getFocalVal
# |----------------------------------------------------------------------------|
    def _getFocalVal(self, band_img, index, ifWrite):
        (imgH, imgW) = band_img.shape
        focusVal = self._autoFocus.\
            AutoFocusDriver(band_img.astype(numpy.uint8),
                            imgW, imgH, index, ifWrite)

        return focusVal
# |-------------------------End of _getFocalVal-------------------------------|

    def printFocusMetric(self):
        yOffset = 32
        write_img = 0
        out_image = self._snapImage()
        resizedimg = self._getBandImage(out_image, yOffset, 1)
        cv2.imwrite('Focusmetric.bmp', resizedimg)
        # 3.) Get the focus value from different algorithms
        curfocusVal = self._getFocalVal(resizedimg, 0, write_img)
        print("Focus Metric", curfocusVal)
        self._fMetricLabel.setText(":"+str(curfocusVal))

# |----------------------------------------------------------------------------|
# _confirmBackground
# |----------------------------------------------------------------------------|
    def _confirmBackground(self, input_img):

        # print('in _confirmBG')
        if(input_img.shape[2] == 3):
            rchannel_mean = numpy.mean(input_img[:, :, 0])
            gchannel_mean = numpy.mean(input_img[:, :, 1])
            bchannel_mean = numpy.mean(input_img[:, :, 2])

            rchannel_std = numpy.std(input_img[:, :, 0])
            gchannel_std = numpy.std(input_img[:, :, 1])
            bchannel_std = numpy.std(input_img[:, :, 2])

            print('BG_ std', rchannel_std, gchannel_std,
                  bchannel_std, rchannel_mean, gchannel_mean, bchannel_mean)
            g_threshold = numpy.array(200.0)
            b_threshold = numpy.array(110.0)
            std_thresh = numpy.array(10)

            # To check if the input image is BG
            if((rchannel_std < std_thresh) and
               (gchannel_std < std_thresh) and
               (bchannel_std < std_thresh) and
               bchannel_mean > b_threshold):
                background_val = 1
                print('BG detected')
            else:
                background_val = 0


#             To check if the input image is BG
#             if((gchannel_mean < g_threshold) and
#                (bchannel_mean < b_threshold)):
#                 background_val = 0
#             else:
#                 background_val = 1

        return background_val

# |--------------------------End of _confirmBackground------------------------|

# |----------------------------------------------------------------------------|
# _getbandImage
# |----------------------------------------------------------------------------|
    def _getBandImage(self, input_image, yoffset, camera=1):

        # For MatrixVision.
#         band_img = (input_image[:, :, 0] +
#                     input_image[:, :, 1] +
#                     input_image[:, :, 2]) / 3

        # For H & E stain
        # band_img = (input_image[:, :, 0] + input_image[:, :, 1])/2

        band_img = (input_image[:, :, int(self.channelforFocus)])

        # band_img = (input_image[:, :, 0] + input_image[:, :, 1])/2
        return band_img

#         # For Ximea
#         if (AppConfig.getCameraName() == "Ximea"):
#             # Use Band Index 8 (9th band)
#             extracted_image = input_image[yoffset:yoffset+self.input_img_ht, 0:self.input_img_wd]
#             band_img = extracted_image[3::self.mosaic_size_rows, 0::self.mosaic_size_cols]
#     #         cv2.imwrite('out.jpg',band_img)
#         # for Matrix vision camera
#         elif(AppConfig.getCameraName() == "MatrixVision"):
#             row_sampling = 1
#             col_sampling = 4
#             band_img = (input_image[:,:,0] +
#                         input_image[:,:,1] +
#                         input_image[:,:,2])/3

# |---------------------------End of _getbandImage----------------------------|

# |----------------------------------------------------------------------------|
# _setFocalVal
# |----------------------------------------------------------------------------|
    def _setFocalVal(self, focalVal):
        self._mmcore.setRelativePosition("ZStage", focalVal)
        try:
            while(self._mmcore.deviceBusy("ZStage")):
                pass
        except Exception as msg:
            print(msg)
        return self._mmcore.getPosition("ZStage")

# |-------------------------End of _setFocalVal-------------------------------|

    def _setExposure(self, exp):
        self._mmcore.setExposure(exp)

    def _setChannel(self, channel):
        self.channelforFocus = channel

# |----------------------------------------------------------------------------|
# _showImage
# |----------------------------------------------------------------------------|
    def _showImage(self, pmap, fValVec, intvalVec):
        for ti in self._blockMetricTextVec:
            self._view.scene().removeItem(ti)
        self._blockMetricTextVec = []
        if self._pmap:
            self._view.scene().removeItem(self._pmap)
        self._pmap = self._view.scene().addPixmap(pmap)
        self._pmap.setZValue(0)
        if len(fValVec) > 0:
            self._fMetricLabel.setText(str(round(fValVec[-1], 2)) +
                                       "("+str(round(
                                           self._focusMaxVec[-1], 2))+")")
            if self._focusMaxVec[-1] < fValVec[-1]:
                self._focusMaxVec = fValVec
            for y in range(4):
                for x in range(4):
                    val = round(self._focusMaxVec[y*4+x] - fValVec[y*4+x], 2)
                    val2 = round(fValVec[y*4+x], 2)
                    val3 = round(intvalVec[y*4+x], 2)
                    ti = self._view.scene().addText(str(val))
                    ti.setPos(x*484+20, y*302+20)
                    ti.setFont(QFont("Times", 48, QFont.Bold))
                    ti2 = self._view.scene().addText(str(val2))
                    ti2.setPos(x*484+200, y*302+140)
                    ti2.setFont(QFont("Times", 48, QFont.Bold))
                    ti3 = self._view.scene().addText(str(val3))
                    ti3.setPos(x*484+400, y*302)
                    ti3.setFont(QFont("Times", 20, QFont.Bold))
                    self._blockMetricTextVec.append(ti)
                    self._blockMetricTextVec.append(ti2)
                    self._blockMetricTextVec.append(ti3)

                    if val < -0.2:
                        ti.setDefaultTextColor(QColor(255, 0, 0, 255))
                    else:
                        if val > 0.2:
                            ti.setDefaultTextColor(QColor(0, 255, 0, 255))
                        else:
                            ti.setDefaultTextColor(QColor(255, 255, 255, 255))
                    ti2.setDefaultTextColor(QColor(255, 255, 255, 255))
# |-------------------------------End of _showImage---------------------------|

# |----------------------------------------------------------------------------|
# _showGrid
# |----------------------------------------------------------------------------|
    def _showGridLines(self):
        pen = QPen()
        pen.setWidth(4)
        self._hLines = []
        self._hLines.append(self._view.scene().addLine(0, 608,
                                                       1936, 608, pen))
        self._hLines.append(self._view.scene().addLine(0, 304,
                                                       1936, 304, pen))
        self._hLines.append(self._view.scene().addLine(0, 912,
                                                       1936, 912, pen))

        self._vLines = []
        self._vLines.append(self._view.scene().addLine(968, 0, 968, 1216, pen))
        for i in range(4):
            self._vLines.append(self._view.scene().addLine(i*484, 0, i*484,
                                                           1216, pen))
        for vLine in self._vLines:
            vLine.setZValue(1)
        for hLine in self._hLines:
            hLine.setZValue(1)
# |--------------------------------End of _showGrid---------------------------|

    def _saveImageToDisk(self):
        self._imageThread.exit()
        imgArr = self._snapImage()
        pimg = Image.fromarray(imgArr.astype(numpy.uint8))
        pimg.save(str(time.time())+".bmp")
        self._imageThread.start()

    def _setColBal(self):
        self._imageThread.exit()
        camDev = self._mmcore.getCameraDevice()
        propName = "Camera/GenICam/AnalogControl/BalanceWhiteAuto"
        self._mmcore.setProperty(camDev, propName, "Off")
        self._mmcore.setProperty(camDev, propName, "Once")

#         propName = "ImageProcessing/ColorTwist/ColorTwistEnable"
#         self._mmcore.setProperty(camDev, propName, "On")

#         propName = "ImageProcessing/ColorTwist/" +\
#                 "ColorTwistInputCorrectionMatrixEnable"
#         self._mmcore.setProperty(camDev, propName, "On")

#         propName = "ImageProcessing/ColorTwist/" +\
#                 "ColorTwistOutputCorrectionMatrixEnable"
#         self._mmcore.setProperty(camDev, propName, "On")

#         propName = "ImageProcessing/ColorTwist/" +\
#                 "ColorTwistOutputCorrectionMatrixMode"
#         self._mmcore.setProperty(camDev, propName, "XYZTosRGB_D50")
        self._snapImage()
        propName = "Camera/GenICam/AnalogControl/BalanceWhiteAuto"
        self._mmcore.setProperty(camDev, propName, "Off")
#         propName = "Camera/GenICam/AnalogControl/BalanceRatioSelector"
#         self._mmcore.setProperty(camDev, propName, "Red")
#
#         # 1.960
#         propName = "Camera/GenICam/AnalogControl/BalanceRatio"
#         self._mmcore.setProperty(camDev, propName, 2.986)
#
#         propName = "Camera/GenICam/AnalogControl/" +\
#                    "BalanceRatioSelector"
#         self._mmcore.setProperty(camDev, propName, "Blue")
#
#         # 1.936
#         propName = "Camera/GenICam/AnalogControl/BalanceRatio"
#         self._mmcore.setProperty(camDev, propName, 1.854)
#
        propName = "ImageProcessing/LUTOperations/LUTEnable"
        self._mmcore.setProperty(camDev, propName, "Off")

        self._imageThread.start()

# |----------------------------------------------------------------------------|
# _snapImage
# |----------------------------------------------------------------------------|
    def _snapImage(self):
        self._mmcore.snapImage()
        img = self._mmcore.getImage()

        imgWidth = self._mmcore.getImageWidth()
        imgHeight = self._mmcore.getImageHeight()

        img2 = img.view(dtype=numpy.uint8)
        imageArr = numpy.zeros((imgHeight, imgWidth, 3),
                               dtype=numpy.uint8)

        row_sampling = 1
        col_sampling = 4

        # Flipped BGR to RGB because display need in RGB format.
        imageArr[:, :, 2] = img2[::row_sampling, ::col_sampling]
        imageArr[:, :, 1] = img2[::row_sampling, 1::col_sampling]
        imageArr[:, :, 0] = img2[::row_sampling, 2::col_sampling]

        return imageArr

# |---------------------------End of _snapImage-------------------------------|


if __name__ == '__main__':
    app = QApplication(sys.argv)
    w = SoftJS(None)
    w.setWindowTitle("StageMovement")
    w.show()
    sys.exit(app.exec_())
