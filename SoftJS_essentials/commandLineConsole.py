'''
Created on 23-Jul-2019

@author: Rameesh A R
'''

import cv2
import sys
import ipdb
import time
import numpy
import MMCorePy as MMCore
# import py_spinAutoFocusPipeline as autoFocus

import py_spinSoftJS_AF as autoFocus
from PIL import Image
from PIL.ImageQt import ImageQt
from multiprocessing import Process

from PyQt5.QtCore import QThread, pyqtSignal, QSize
from PyQt5.QtGui import QIcon, QPixmap, QPen, QFont, QColor
from PyQt5.QtWidgets import QWidget, QPushButton, QGridLayout, QApplication,\
    QSpinBox, QLabel, QDoubleSpinBox, QGraphicsScene, QCheckBox,\
    QMessageBox, QLineEdit, QPlainTextEdit

from SnapThread import SnapThread
from ZoomGraphicsView import ZoomGraphicsView
from os.path import join
from PyQt5 import QtCore, QtGui


class EmittingStream(QtCore.QObject):
    '''Statement.

    Description Line1
    .
    .
    .
    Description LineN
    '''
# |----------------------------------------------------------------------------|
# class Variables
# |----------------------------------------------------------------------------|
    textWritten = QtCore.pyqtSignal(str)

# |----------------------------------------------------------------------------|
# write
# |----------------------------------------------------------------------------|
    def write(self, text):
        self.textWritten.emit(str(text))
# |----------------------End of write---------------------------|


# ==============================================================================
# CommandLineModel
# ==============================================================================
class CommandLineModel(QWidget):
    '''
    This sends the command to the controller card which we input
    '''
# |----------------------------------------------------------------------------|
# class Variables
# |----------------------------------------------------------------------------|
    # no classVariables
    ANSWER_TIME_OUT = "1000"

    BAUD_RATE = "230400"

    DELAY_BETWEEN_CHARS_MS = "0"

    HANDSHAKING = "Off"

    PARITY = "None"

    STOP_BITS = 1

# |----------------------------------------------------------------------------|
# Constructor
# |----------------------------------------------------------------------------|
    def __init__(self, parent):
        QWidget.__init__(self, parent)
        self._mmcore = MMCore.CMMCore()
        self._propList = [("AnswerTimeout", self.ANSWER_TIME_OUT),
                          ("BaudRate", self.BAUD_RATE),
                          ("DelayBetweenCharsMs", self.DELAY_BETWEEN_CHARS_MS),
                          ("Handshaking", self.HANDSHAKING),
                          ("Parity", self.PARITY),
                          ("StopBits", self.STOP_BITS)]
        self.commandLabel = QLabel('Command: ')
        self.commandLine = QLineEdit()
        self.sendButton = QPushButton("send")
        self.textBox = QPlainTextEdit()
        self.connectSystem = QPushButton("Connect")
        self.connectSystem.setCheckable(True)
        self.connectSystem.setChecked(False)
#         self.setMaximumSize(QSize(900, 900))
        self.setUpGui()
        self.setUpConnections()
# |-------------------------End of Constructor------------------------------|

# |----------------------------------------------------------------------------|
# setUpGui
# |----------------------------------------------------------------------------|
    def setUpGui(self):
        self.setLayout(QGridLayout(self))
#         self.textBox.setReadOnly(True)
        self.layout().addWidget(self.connectSystem, 0, 0)
        self.layout().addWidget(self.commandLabel, 1, 0)
        self.layout().addWidget(self.commandLine, 1, 1)
        self.layout().addWidget(self.sendButton, 1, 2)
        self.layout().addWidget(self.textBox, 2, 0, 2, 2)
# |----------------------End of setUpGui---------------------------|

# |----------------------------------------------------------------------------|
# setUpConnections
# |----------------------------------------------------------------------------|
    def setUpConnections(self):
        self.sendButton.pressed.connect(self.sendCommand)
        self.connectSystem.pressed.connect(self.connectStage)
# |----------------------End of setUpConnections---------------------------|

# |----------------------------------------------------------------------------|
# Destructor
# |----------------------------------------------------------------------------|
    def __del__(self):
        # Restore sys.stdout
        sys.stdout = sys.__stdout__
# |-------------------------End of Destructor------------------------------|

# |----------------------------------------------------------------------------|
# consoleOutput
# |----------------------------------------------------------------------------|
    def consoleOutput(self, text):
        cursor = self.textBox.textCursor()
        cursor.movePosition(QtGui.QTextCursor.End)
        cursor.insertText(text)
        self.textBox.setTextCursor(cursor)
        self.textBox.ensureCursorVisible()
# |----------------------End of consoleOutput-----------------------|

# |----------------------------------------------------------------------------|
# sendCommand
# |----------------------------------------------------------------------------|
    def sendCommand(self):
        sys.stdout = EmittingStream(
            textWritten=self.consoleOutput)
        if self.connectSystem.isChecked():
            command = str(self.commandLine.text())
            print(command)
            self._mmcore.setProperty("XYStage", "SendDesiredCommand", command)
            self._mmcore.setProperty("XYStage", "EnableCommandLine", "ON")
        else:
            self._mmcore.setProperty("XYStage", "EnableCommandLine", "OFF")
# |----------------------End of sendCommand---------------------------|

# |----------------------------------------------------------------------------|
# _initDevice
# |----------------------------------------------------------------------------|
    def _initDevice(self, devName, libName, propList=None):
        # First load the device.
        self._mmcore.loadDevice(devName, libName, devName)
        sys.stdout = EmittingStream(
            textWritten=self.consoleOutput)
        # Set the properties if any.
        if propList is not None:
            for propInfo in propList:
                propName = propInfo[0]
                propVal = propInfo[1]

                # Doing try catch because some properties might not be
                # modifiable.
                try:
                    self._mmcore.setProperty(devName, propName, propVal)
                except Exception:
                    pass

        # Initialize device.
        try:
            self._mmcore.initializeDevice(devName)
            if "TOFRA" in devName or "ZStage" in devName:
                self._mmcore.setFocusDevice(devName)
            return True, ""
        except Exception as msg:
            return False, msg
# |----------------------End of _initDevice---------------------------|

# |----------------------------------------------------------------------------|
# _initComPort
# |----------------------------------------------------------------------------|
    def _initComPort(self, comPortName, comPortPropList):
        # Load and initialize COM port.
        sys.stdout = EmittingStream(
            textWritten=self.consoleOutput)
        self._mmcore.loadDevice(comPortName, "SerialManager", comPortName)
        try:
            for propInfo in comPortPropList:
                propName = propInfo[0]
                propVal = propInfo[1]
                # Doing try catch because some properties might not be
                # modifiable.
                try:
                    self._mmcore.setProperty(comPortName, propName, propVal)
                except Exception as msg:
                    print("Exception occurred in "
                          "HardwareManager::initComPort: ", msg)

            self._mmcore.initializeDevice(comPortName)

            return True, ""
        except Exception as msg:
            return False, msg
# |----------------------End of _initComPort---------------------------|

# |----------------------------------------------------------------------------|
# connectStage
# |----------------------------------------------------------------------------|
    def connectStage(self):
        sys.stdout = EmittingStream(
            textWritten=self.consoleOutput)
        self._mmcore.unloadAllDevices()
        retVal = False
        portNameList = self._mmcore.getAvailableDevices("SerialManager")
        for comPort in portNameList:
            print("Trying COM Port: " + str(comPort))
            try:
                self._initComPort(comPort, self._propList)
                propInfo = ("Port", comPort)
                propList = []
                propList.append(propInfo)
                self._initDevice("Scope", "VulcanScopeMod", propList)
                self._initDevice("XYStage", "VulcanScopeMod")
                self._initDevice("ZStage", "VulcanScopeMod")
                self._initDevice("XYZCom", "VulcanScopeMod")
                self._initDevice("WhiteLamp", "VulcanScopeMod")
                self._initDevice("JoyStick", "VulcanScopeMod")
                print(self._mmcore.getAvailableDevices("VulcanScopeMod"))
                self._initDevice("ObjectiveLens", "VulcanScopeMod")
                self._mmcore.setProperty("XYZCom", "EnableXYZ", "ON")
                self._mmcore.setProperty("XYZCom", "SetLimitX", 200000)
                self._mmcore.setProperty("XYZCom", "SetLimitY", 200000)
                self._mmcore.setProperty("XYZCom", "SetLimitZ", 232000)
                self._mmcore.setProperty("XYZCom", "EnableSetLimitXYZ", "ON")
                self._mmcore.setProperty("XYZCom", "MicroStepX", 8)
                self._mmcore.setProperty("XYZCom", "MicroStepY", 8)
                self.zMicroStepping = 16
                self._mmcore.setProperty("XYZCom", "MicroStepZ",
                                         self.zMicroStepping)
                self._mmcore.setProperty("XYZCom", "MicroStepTurret", 8)
                self._mmcore.setProperty("XYZCom", "XStageMode", 3)
                self._mmcore.setProperty("XYZCom", "YStageMode", 3)
                self._mmcore.setProperty("XYZCom", "ZStageMode", 3)
                self._mmcore.setProperty("XYZCom", "TurretMode", 3)
                self._mmcore.setProperty("XYZCom", "EnableMicroStepXYZ", "ON")
                self._mmcore.setProperty("ZStage", "BacklashZ", 0)
                print(self._mmcore.getProperty("ZStage", "BacklashZ"))
                retVal = True
                break
            except Exception as msg:
                retVal = False
                print("Exception occurred in "
                      "AutoConfigureLabEndScope::initStage: ", msg)
        self._mmcore.setProperty("XYStage", "SpeedXY", 10)
        self._mmcore.setProperty("ZStage", "SpeedZ", 0.5)
        print("Connected")
        print("White Lamp Props: ",
              self._mmcore.getDevicePropertyNames("WhiteLamp"))
        print("Prop Vals: ", self._mmcore.getAllowedPropertyValues(
            "WhiteLamp",
            "SetLedPower"))
        stepSize = float(self._mmcore.getProperty("XYStage",
                                                  "GetStepSizeXYUm"))
        print('micronPerSteps', stepSize)
        print(retVal)
        return retVal
# |---------------------------------End of connectStage----------------------|


if __name__ == '__main__':
    app = QApplication(sys.argv)
    w = CommandLineModel(None)
    w.setWindowTitle("Command Line Console")
    w.show()
    sys.exit(app.exec_())
