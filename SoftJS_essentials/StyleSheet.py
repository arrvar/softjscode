__author__ = "Smit Patel"
__credits__ = ["Suhash Gerald"]
__copyright__ = "Copyright 2016, Spectral Insights Pvt. Ltd."
__status__ = "Development"


# ==============================================================================
# StyleSheet
# ==============================================================================

class StyleSheet():
    '''Qt StyleSheet block for complete view part'''

# |----------------------------------------------------------------------------|
# class Variables
# |----------------------------------------------------------------------------|
    mainBackground = "background-color:rgb(39, 39, 39)"
    whiteBackground = "background-color:rgb(255, 255, 255)"
    whiteFontColor = "color:#eeeeee"
    borderBottm = " border-bottom: 3px solid white"
    blackBG = "background-color: #000000"
    workspaceButton = """
                        QPushButton {
                            color:rgb(240, 240, 240);
                            background-color:rgb(43, 61, 75);
                            border:0px;
                        }

                        QPushButton:hover {
                            color:rgb(240, 240, 240);
                            background-color:rgb(50, 50, 50);
                            border:0px;
                        }

                        QPushButton:pressed {
                            color:rgb(240, 240, 240);
                            background-color:rgb(100, 100, 100);
                            border:0px;
                        }

                        QToolTip {
                            color:rgb(0, 0, 0);
                            background-color:(249, 249, 252);
                        }

                    """

    defaultButton = """
                        QPushButton {
                            color:rgb(240, 240, 240);
                            background-color:rgb(64, 64, 64);
                            border:0px;
                            font: 75 8pt "Arial";
                        }

                        QPushButton:hover {
                            color:rgb(240, 240, 240);
                            background-color:rgb(64, 112, 151);
                            border:0px;
                            font: 75 8pt "Arial";
                        }

                        QPushButton:pressed {
                            color:rgb(48, 48, 48);
                            background-color:rgb(81, 176, 255);
                            border:0px;
                            font: 75 8pt "Arial";
                        }

                        QPushButton:disabled {
                            color:rgb(135, 135, 135);
                            background-color:rgb(64, 64, 64);
                            border:0px;
                            font: 75 8pt "Arial";
                        }

                        QToolTip {
                            color:rgb(0, 0, 0);
                            background-color:(249, 249, 252);
                        }
                    """

    lineEdit = """
                    QLineEdit {
                        color:rgb(240, 240, 240);
                        background-color:rgb(64, 64, 64);
                        selection-background-color:(64, 112, 151);
                        border:0px;
                        font: 75 8pt "Arial";
                    }
                """

    label = """
                QLabel {
                    color:rgb(240, 240, 240);
                    font: 75 9pt "Arial";
                }
            """

    msgBoxBorder = "border:1px solid rgb(0, 255, 0);"
    scrollBar = """
                QScrollBar:vertical
                {
                    border: 1px solid #999999;
                    background:rgb(255, 255, 255);
                    width:10px;
                    margin: 0px 0px 0px 0px;
                }
                QScrollBar:horizontal
                {
                    border: 1px solid #999999;
                    background:rgb(255, 255, 255);
                    height:10px;
                    margin: 0px 0px 0px 0px;
                }
                QScrollBar::handle
                {
                    background: qlineargradient(x1:0, y1:0, x2:1, y2:0,
                    stop: 0  rgb(33, 33, 33), stop: 0.5 rgb(33,33,33), stop:1 rgb(33,33,33));
                    min-height: 0px;
                }
                QScrollBar::add-line
                {
                    background: qlineargradient(x1:0, y1:0, x2:1, y2:0,
                        stop: 0  rgb(33,33,33), stop: 0.5 rgb(33,33,33), stop:1 rgb(33,33,33));
                }
                QScrollBar::sub-line
                {
                    background: qlineargradient(x1:0, y1:0, x2:1, y2:0,
                        stop: 0  rgb(33, 33, 33), stop: 0.5 rgb(33,33,33), stop:1 rgb(33,33,33));
                }"""

    headerView = """
                    QHeaderView::section {
                        background-color: qlineargradient(x1:0, y1:0,
                        x2:0, y2:1, stop:0 #616161, stop: 0.5 #505050,
                        stop: 0.6 #434343, stop:1 #656565);
                        color: white;
                        padding-left: 4px;
                        border: 1px solid #6c6c6c;
                    }
                """

    PushButton = """
                    QPushButton {
                        border:0px;
                    }

                    QPushButton:hover {
                        background-color:rgb(64, 112, 151);
                        border:0px;
                    }

                    QPushButton:pressed {
                        background-color:rgb(81, 176, 255);
                        border:0px;
                    }

                    QToolTip {
                        color:rgb(0, 0, 0);
                        background-color:(249, 249, 252);
                    }
                """

    ComboBox = """
                    QComboBox {
                        background-color:rgb(64, 64, 64);
                        border:0px;
                    }

                    QComboBox:hover {
                        background-color:rgb(64, 112, 151);
                    }

                    QComboBox::drop-down {
                        border:0px;
                    }

                    QComboBox::down-arrow {
                        image: url(./images/drop_down.png);
                    }
               """

    TreeView = """
                    QTreeView::item:hover {
                        background-color:rgb(64, 112, 151);
                    }

                    QTreeView::item:selected {
                        color:black;
                        background-color:rgb(81, 176, 255);
                    }

                    QTreeView::branch:has-siblings:!adjoins-item {
                        border-image:url(./images/treeview_vline.png) 0;
                    }

                    QTreeView::branch:has-siblings:adjoins-item {
                        border-image:url(./images/treeview_branch_more.png) 0;
                    }

                    QTreeView::branch:!has-children:!has-siblings:adjoins-item {
                        border-image:url(./images/treeview_branch_end.png) 0;
                    }

                    QTreeView::branch:has-children:!has-siblings:closed,
                    QTreeView::branch:closed:has-children:has-siblings {
                        border-image: none;
                        image:url(./images/treeview_branch_closed.png);
                    }

                    QTreeView::branch:open:has-children:!has-siblings,
                    QTreeView::branch:open:has-children:has-siblings  {
                        border-image: none;
                        image:url(./images/treeview_branch_open.png);
                    }

                    QTreeView:disabled {
                            color:rgb(135, 135, 135);
                            background-color:rgb(64, 64, 64);
                    }

                    QToolTip {
                        color:rgb(0, 0, 0);
                        background-color:(249, 249, 252);
                    }

               """

    TableView = """
                    QTableWidget {
                        gridline-color:rgb(135, 135, 135);
                    }
                """

    ProgressDialog = """
                        QProgressBar {
                            color:white;
                            border: 0px;
                            text-align: center;
                        }

                        QProgressBar::chunk {
                            background-color:rgb(81, 176, 255);
                        }
                    """

    IconButton = """
                    QPushButton {
                        border:0px;
                    }

                    QPushButton:hover {
                        background-color:rgb(64, 112, 151);
                        border:0px;
                    }

                    QPushButton:pressed {
                        background-color:rgb(81, 176, 255);
                        border:0px;
                    }

                    QPushButton:checked {
                        background-color:rgb(81, 176, 255);
                        border:0px;
                    }

                    QToolTip {
                        color:rgb(0, 0, 0);
                        background-color:(249, 249, 252);
                    }
                """

    CheckBox = """
                    QCheckBox {
                        color:rgb(240, 240, 240);
                        border:0px;
                        font: 75 8pt "Arial";
                    }

                    QCheckBox:hover {
                        color:rgb(240, 240, 240);
                        border:0px;
                        font: 75 8pt "Arial";
                    }

                    QCheckBox:disabled {
                        color:rgb(135, 135, 135);
                        border:0px;
                        font: 75 8pt "Arial";
                    }

                    QToolTip {
                        color:rgb(0, 0, 0);
                        background-color:(249, 249, 252);
                    }
                """
    Label = """
                QLabel {
                        font: 20 15pt "Arial";
                        }
            """
