import numpy
from PyQt5.QtCore import pyqtSignal, QObject, pyqtSlot, QMutex


# ==============================================================================
# SnapThread
# ==============================================================================
class SnapThread(QObject):
    '''
    Class for capturing images during continuous movement of XY-Stage.
    '''
# |----------------------------------------------------------------------------|
# class Variables
# |----------------------------------------------------------------------------|
    # Image.
    capturedImgArr = pyqtSignal(numpy.ndarray)

    processFinished = pyqtSignal()

    processAborted = pyqtSignal()

    processError = pyqtSignal()

# |----------------------------------------------------------------------------|
# Constructor
# |----------------------------------------------------------------------------|
    def __init__(self, mmcore=None):
        QObject.__init__(self)
        self._mmcore = mmcore
        self._imageCount = 0
        self._abortMutex = QMutex()
        self._abort = False

# |-------------------------End of Constructor--------------------------------|

# |----------------------------------------------------------------------------|
# onStartProcess
# |----------------------------------------------------------------------------|
    @pyqtSlot()
    def onStartProcess(self):
        err = False
        for counter in range(self._imageCount):
            try:
                # start = time.time()
                self._mmcore.snapImage()
                imgArr = self._mmcore.getImage()
                # end = time.time()
                # print("Snap Time: ", (end - start))
                self.capturedImgArr.emit(imgArr)
            except Exception as ex:
                print("Snap missed: ", ex, counter)
                err = True
                break
            if self._abort is True:
                break

        self.thread().quit()

        if err is False:
            if self._abort is True:
                self.processAborted.emit()
            else:
                self.processFinished.emit()
        else:
            if self._abort is True:
                self.processAborted.emit()
            else:
                self.processError.emit()

# |---------------------------End of onStartProcess---------------------------|

# |----------------------------------------------------------------------------|
# onStopProcess
# |----------------------------------------------------------------------------|
    @pyqtSlot()
    def onStopProcess(self):
        self._abortMutex.lock()
        self._abort = True
        self._abortMutex.unlock()

# |---------------------------End of onStopProcess----------------------------|
